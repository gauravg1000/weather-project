Go version `1.14`

###How to Set Up Environment for running the application ?

2 environment variables have been added in the .env file.

ACCESS_KEY refers to the weather stack access key that you can get by SignIn/SignUp on www.weatherstack.com
APP_ID refers to the open weather map access key that you can get by Signin/SignUp on www.openweathermap.org

Get the access keys for both the APIs and add them to the .env file

For Example : If access key for weather stack is 0187295ee1338f54721fe39c7c1e6de3 then set

ACCESS_KEY="0187295ee1338f54721fe39c7c1e6de3"

Same needs to done for the other key.

###How to build and Run?

1. Build the binary `go build main.go`

2. Run the binary `./main`

3. curl `http://localhost:8080/v1/weather?city=melbourne` using the command line or any rest client like POSTMAN.
