package middleware

import (
	"bitbucket.org/weather-project/logger"
	"bitbucket.org/weather-project/models"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"
)

//cache variable of type map to store the cache weather of city
var cache = make(map[string]models.Cache)

//resp variable to store the response data which is returned as json to the api call
var resp models.Response

func GetWeather(w http.ResponseWriter, r *http.Request) {
	var cacheAdd models.Cache
	var failCount int

	city := r.URL.Query()
	cities := city["city"]

	epoch := time.Now().Unix()

	if cache[cities[0]].Time+3 >= epoch {
		logger.WriteLog("request served by cache")
		resp.WindSpeed = cache[cities[0]].Response.WindSpeed
		resp.Temperature = cache[cities[0]].Response.Temperature
		cacheAdd.Response = resp
		now := time.Now().Unix()
		cacheAdd.Time = now
		cache[cities[0]] = cacheAdd
	} else {
		response, err := getWeatherStack(cities[0])
		if err != nil {
			logger.WriteLog(err)
			failCount++
			response , err = getOpenWeatherMap()
			if err != nil {
				failCount++
				logger.WriteLog(err)
			} else {
				logger.WriteLog("response served by Open Weather Map API")
				resp.WindSpeed = response.WindSpeed
				resp.Temperature = response.Temperature
				cacheAdd.Response = resp
				now := time.Now().Unix()
				cacheAdd.Time = now
				cache[cities[0]] = cacheAdd
			}
		} else {
			logger.WriteLog("response served by Weather Stack API")
			resp.WindSpeed = response.WindSpeed
			resp.Temperature = response.Temperature
			cacheAdd.Response = resp
			now := time.Now().Unix()
			cacheAdd.Time = now
			cache[cities[0]] = cacheAdd
		}
		if failCount == 2 {
			if val, ok := cache[cities[0]]; ok {
				resp.WindSpeed = val.Response.WindSpeed
				resp.Temperature = val.Response.Temperature
				cacheAdd.Response = resp
				now := time.Now().Unix()
				cacheAdd.Time = now
				cache[cities[0]] = cacheAdd
			} else {
				// todo If both the APIs fail and there is no data for the requested city in cache then we can handle it according to our usecase.
				logger.WriteLog("unable to get weather data from the weather APIs and data for city is not available in the cache too")
				err := json.NewEncoder(w).Encode("unable to get weather data from the weather APIs and data for the requested city is not available in the cache too")
				if err != nil {
					logger.WriteLog("error sending the response", err)
					return
				}
				return
			}
		}
	}
	err := json.NewEncoder(w).Encode(resp)
	if err!= nil{
		logger.WriteLog("error sending the response", err)
		return
	}
}

//getWeather function to get the weather data from weather stack api
//it accepts the city name for which the weather data needs to be fetched in the string data type
//it returns a structure of response and error
//incase of any error the return values are empty structure along with the error
func getWeatherStack(city string) (models.Response, error){
	var weatherStack models.WeatherStack
	var weatherStackError models.WeatherStackError

	client := &http.Client{}
	req,err := http.NewRequest("GET","http://api.weatherstack.com/current", nil)
	if err != nil {
		logger.WriteLog("error while creating the http request for weather stack")
		return resp, err
	}

	args := req.URL.Query()
	args.Add("access_key", os.Getenv("ACCESS_KEY"))
	args.Add("query",city)

	req.URL.RawQuery = args.Encode()
	response,err := client.Do(req)
	if err != nil {
		logger.WriteLog("error with http request to weather stack")
		return resp,err
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.WriteLog("error reading the response body of weather stack")
		return resp,err
	}

	err = json.Unmarshal(body,&weatherStack)
	if err != nil || (weatherStack.Request.Type == "") {
		logger.WriteLog("error unmarshalling the weather stack data")
		err = json.Unmarshal(body,&weatherStackError)
			if err != nil {
				logger.WriteLog(err)
			} else {
				logger.WriteLog(weatherStackError.Error.Info)
				return resp, errors.New("error in weather stack API")
			}
	}

	resp.WindSpeed = weatherStack.Current.WindSpeed
	resp.Temperature = weatherStack.Current.Temperature
	return resp,nil
}

//getOpenWeatherMap function to get the weather data from open weather map api
//it doesn't need any parameter at the function call
//it returns a structure of response and error
//incase of any error the return values are empty structure along with the error
func getOpenWeatherMap() (models.Response, error){
	var openWeatherMap models.OpenWeatherMap

	client := &http.Client{}
	req, err := http.NewRequest("GET","http://api.openweathermap.org/data/2.5/weather",nil)
	if err != nil {
		logger.WriteLog("error while creating the http request for open weather map")
		return resp, err
	}
	args := req.URL.Query()
	args.Add("q", "melbourne,AU")
	args.Add("appid", os.Getenv("APP_ID"))
	args.Add("units", "metric")

	req.URL.RawQuery = args.Encode()
	response,err := client.Do(req)
	if err != nil {
		logger.WriteLog("error with http request to open weather map")
		return resp,err
	}

	if response.StatusCode != http.StatusOK {
		logger.WriteLog("error with the http request for open weather api")
		return resp, errors.New("error in the open weather map api request status code: " + strconv.Itoa(response.StatusCode))
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logger.WriteLog("error reading the response body of open weather map")
		return resp,err
	}
	err = json.Unmarshal(body,&openWeatherMap)
	if err != nil {
		logger.WriteLog("error unmarshalling the open weather map data")
		return resp,err
	}

	resp.WindSpeed = openWeatherMap.Wind.Speed
	resp.Temperature = openWeatherMap.Main.Temp
	return resp,nil
}
