package routers

import (
	"github.com/gorilla/mux"
	"bitbucket.org/weather-project/middleware"
)

func Router() *mux.Router {

	router := mux.NewRouter()

	router.HandleFunc("/v1/weather", middleware.GetWeather).Methods("GET")

	return router
}
