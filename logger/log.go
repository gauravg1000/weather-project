package logger

import (
"fmt"
"time"
)

//WriteLog function to write the log with the current timestamp
func WriteLog(logs ...interface{}){
	fmt.Println(getTimeStamp(),logs)
}

func getTimeStamp()string{
	return fmt.Sprint(time.Now().Format(time.RFC1123)+":\t")
}
