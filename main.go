package main

import (
	"bitbucket.org/weather-project/logger"
	"bitbucket.org/weather-project/routers"
	"github.com/joho/godotenv"
	"github.com/rs/cors"
	"log"
	"net/http"
)

func init(){
	godotenv.Load(".env")
}

func main() {
	logger.WriteLog("service started")

	//create router
	r := routers.Router()

	handler := cors.Default().Handler(r)

	log.Fatal(http.ListenAndServe(":8080", handler))
}
