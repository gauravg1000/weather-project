module bitbucket.org/weather-project

go 1.14

require (
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/rs/cors v1.7.0
)
