package models


type OpenWeatherMap struct {
Coord Coord
Weather []Weather
Base string
Main Main
Visibility int
Wind Wind
Rain Rain
Clouds Clouds
Dt int64
Sys Sys
Timezone int
Id int
Name string
Cod int
}

type Coord struct {
	lon float64
	lat float64
}

type Weather struct {
	Id int
	Main string
	Description string
	Icon string
}

type Main struct {
	Temp float64
	FeelsLike float64  `json:"feels_like"`
	TempMin float64	   `json:"temp_min"`
	TempMax float64	   `json:"temp_max"`
	Pressure int
	Humidity int
}

type Wind struct {
	Speed float64
	Deg int
}

type Rain struct {
	Rain float64  `json:"1h"`
}

type Clouds struct {
	Clouds int  `json:"all"`
}

type Sys struct {
	Type int
	Id int
	Country string
	Sunrise int64
	Sunset int64

}