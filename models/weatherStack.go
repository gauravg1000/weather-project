package models

type WeatherStack struct{
	Request Request
	Location Location
	Current Current
}

type Request struct {
	Type string `json:"type"`
	Query string
	Language string
	Unit string
}

type Location struct {
	Name string
	Country string
	Region string
	Lat string
	Lon string
	TimezoneId string
	Localtime string
	LocaltimeEpoch int64
	UtcOffset string
}

type Current struct {
	ObservationTime string
	Temperature float64
	WeatherCode int
	WeatherIcons []string
	WeatherDescriptions []string
	WindSpeed float64     `json:"wind_speed"`
	WindDegree int
	WindDir string
	Pressure int
	Precip int
	Humidity int
	CloudCover int
	Feelslike int
	UvIndex int
	Visibility int
	IsDay string
}

type WeatherStackError struct {
	Success bool
	Error Error
}

type Error struct {
	Code int
	Info string
}