package models

type Response struct {
	WindSpeed float64 `json:"wind_speed"`
	Temperature float64 `json:"temperature_degrees"`
}

type Cache struct {
	Response Response
	Time int64
}
